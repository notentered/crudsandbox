package com.zlatko.crudsandbox.sandbox;

import org.springframework.roo.addon.dod.RooDataOnDemand;

@RooDataOnDemand(entity = Something.class)
public class SomethingDataOnDemand {
}
