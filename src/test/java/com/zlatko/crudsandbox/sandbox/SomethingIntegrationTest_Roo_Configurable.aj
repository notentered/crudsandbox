// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.zlatko.crudsandbox.sandbox;

import com.zlatko.crudsandbox.sandbox.SomethingIntegrationTest;
import org.springframework.beans.factory.annotation.Configurable;

privileged aspect SomethingIntegrationTest_Roo_Configurable {
    
    declare @type: SomethingIntegrationTest: @Configurable;
    
}
