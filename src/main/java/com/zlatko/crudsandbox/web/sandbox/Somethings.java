package com.zlatko.crudsandbox.web.sandbox;

import com.zlatko.crudsandbox.sandbox.Something;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/somethings")
@Controller
@RooWebScaffold(path = "somethings", formBackingObject = Something.class)
public class Somethings {
}
